from django.db import models

# Create your models here.


class Articles(models.Model):
	author         = models.CharField(max_length=255)
	email          = models.EmailField()
	phone          = models.CharField(max_length=13)
	title          = models.CharField(max_length=200)
	description    = models.TextField(null=True, blank=True)
	body           = models.TextField(null=True, blank=True)
	image 		   = models.ImageField(upload_to='doc_images/',null=True, blank=True)
	rating         = models.IntegerField(null=True, blank=True)
	likes          = models.IntegerField(default=0)

	def __str__(self):
		return str(self.title)[:15]


	class Meta:
		verbose_name_plural = 'Articles'

