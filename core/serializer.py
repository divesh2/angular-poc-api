from rest_framework import serializers
from .models import Articles


class SnippetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articles
        fields = [ 'id' ,'author','email', 'image' ,'phone','title' ,'description', 'body']