from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
from .models import Articles
from .serializer import SnippetSerializer
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from rest_framework import status
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt



class ListArticles(APIView):
	parser_classes = [MultiPartParser]

	def get(self, request, format=None):
		queryset = Articles.objects.all()
		serializer = SnippetSerializer(queryset, many=True)
		return Response(serializer.data)

	def post(self, request, format=None):
		print(request.data)
		print(request.POST)
		print(request.FILES)
		data = {x:y[0] for x,y in dict(request.POST).items()}
		try:
			data['image'] = request.FILES['image']
		except:
			data['image'] = None
		serializer = SnippetSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data['id'])
		else:
			print(serializer.errors)
		return JsonResponse({'status': 'Not Done'})

# @csrf_exempt
# def file(request):
# 	print(request.data)
# 	print(request.POST)
# 	print(request.FILES)
# 	return JsonResponse({'status': 'Ok'})


class Article(APIView):

	def get_object(self, pk):
		try:
			return Articles.objects.get(pk=pk)
		except Articles.DoesNotExist:
			raise Http404

	def get(self, request, pk, format=None):
		snippet = self.get_object(pk)
		serializer = SnippetSerializer(snippet)
		return Response(serializer.data)


	def delete(self, request, pk, format=None):
		snippet = self.get_object(pk)
		snippet.delete()
		return JsonResponse({'status': 'Done'}, status=status.HTTP_204_NO_CONTENT)