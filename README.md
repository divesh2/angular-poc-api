##Dependencies:

1. Python 3.8+
2. Django 3.1  (pip install django==3.1)
3. Django Rest Framework (pip install djangorestframework)
4. djangorestframework-simplejwt  (pip install djangorestframework-simplejwt)
5. cors-header (pip install django-cors-headers)


##Command to run the project: 

python manage.py runserver


##Admin Section:

127.0.0.1:8000/admin

##Admin credentials:

username:  'golu'
password:   'q'
